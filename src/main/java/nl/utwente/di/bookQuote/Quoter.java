package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> convertedCelsius;
    //comment
    public Quoter() {
        convertedCelsius = new HashMap<>();
        convertedCelsius.put("1", 33.8);
        convertedCelsius.put("2", 35.6);
        convertedCelsius.put("3", 37.4);
        convertedCelsius.put("4", 39.2);
        convertedCelsius.put("5", 41.0);

    }

    public double celsiusConv(double celsius) {
        return (celsius * 9/5) + 32;
    }
}
